import { StatusBar } from 'expo-status-bar';
import React, { useState } from "react";
import { StyleSheet, Text, View, Button } from 'react-native';
import { WebView, WebViewNavigation } from 'react-native-webview';
import axios from 'axios';
import { render } from 'react-dom';

const url = 'https://api.playground.klarna.com/checkout/v3/orders'
const geturl = 'https://api.playground.klarna.com/checkout/v3/orders/'

//postrequest with axios to create order

const token = 'UEs0MDE1OV9jNmJlYTQ3YWNmMjk6WVFTWHpSMEp1eklQOURwcQ=='

const headers = {
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Basic ${token}`
  }
}



const data = {
  "purchase_country": "SE",
  "purchase_currency": "SEK",
  "locale": "sv-SE",
  "order_amount": 50000,
  "order_tax_amount": 4545,
  "order_lines": [
      {
          "type": "physical",
          "reference": "19-402-USA",
          "name": "Red T-Shirt",
          "quantity": 5,
          "quantity_unit": "pcs",
          "unit_price": 10000,
          "tax_rate": 1000,
          "total_amount": 50000,
          "total_discount_amount": 0,
          "total_tax_amount": 4545
      }
      ],
  "merchant_urls": {
      "terms": "https://www.example.com/terms.html",
      "checkout": "https://www.example.com/checkout.html",
      "confirmation": "https://localhost:44346/Identity/Account/Login",
      "push": "https://www.example.com/api/push"
  }
}



export default function App() {
  const [renderView, setRenderView] = useState('https://www.google.com');
  const [isLoading, setIsLoading] = useState(false);
  const [orderId, setOrderId] = useState('');

  // const html = `<textarea style="display: none" id="KCO">
  // ${renderView} </textarea>
  //   <div id="my-checkout-container"></div>
  // <!-- START - Dont edit -->
  // <script type="text/javascript">
  //   var checkoutContainer = document.getElementById('my-checkout-container')
  //   checkoutContainer.innerHTML = (document.getElementById("KCO").value).replace(/\\"/g, "\"").replace(/\\n/g, "");
  //   var scriptsTags = checkoutContainer.getElementsByTagName('script')
  //   for (var i = 0; i < scriptsTags.length; i++) {
  //     var parentNode = scriptsTags[i].parentNode
  //     var newScriptTag = document.createElement('script')
  //     newScriptTag.type = 'text/javascript'
  //     newScriptTag.text = scriptsTags[i].text
  //     parentNode.removeChild(scriptsTags[i])
  //     parentNode.appendChild(newScriptTag)
  //   }
  // </script>`

  
  //Webview klarna checkout
  // const jsCode = 
  // `
  // window._klarnaCheckout(function(api) {
  //   api.on({
  //     'redirect_initiated': function(data) {
  //       alert("redirect initiated");
  //     }
  //   });
  // });
  // `

  const jsCode = 
  `
  window._klarnaCheckout(function(api) {
       api.on({
         'redirect_initiated': function(data) {
           window.ReactNativeWebView.postMessage(data);
         }
       });
     });
  `

  // const jsCode = `
  //   window.postMessage("Your message");
  // `;


  const onSubmitFormHandler = async (event) => {
    setIsLoading(true);
    try {
      const response = await axios.post(`${url}`, data, headers);
      if (response.status === 201) {
        setOrderId(geturl+response.data.order_id);
        setRenderView(response.data.html_snippet);
        // setRenderView("<h3>hejsan</h3>")
        setIsLoading(false);
      } else {
        alert(error.response)
      }
    } catch (error) {
      setIsLoading(false);
    }

    alert(html)
  };

  return (
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your aaa!</Text>
      
    //   <Button
    //         title="Submit"
    //         onPress={onSubmitFormHandler}
    //         disabled={isLoading}
    //       />
    // </View>
    <View style={{height: 800, paddingTop: 40}}>
    <Button
            title="Submit"
            onPress={onSubmitFormHandler}
            disabled={isLoading}
          />
    <WebView
      javaScriptEnabled={true}
      originWhitelist={["*"]}
      userAgent="Mozilla/5.0 (Linux; Android 4.4.4; One Build/KTU84L.H4) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/28.0.0.20.16;]"
      onNavigationStateChange={(navState) => {
        // Keep track of going back navigation within component
        this.canGoBack = navState.canGoBack;
      }}
      injectedJavaScript={jsCode}
      onMessage={event => {
        alert('MESSAGE >>>>' + event.nativeEvent.data);
      }}
      source={{ html: renderView }}
      style={{flex:1, height: 1}} 
      />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
